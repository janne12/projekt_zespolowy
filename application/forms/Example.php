<?php

class Application_Form_Example extends Zend_Form
{

    public function init()
    {
        $this->setName('ex');
        $this->setMethod('post');
        
        $html = new Zend_Form_Element_Textarea('html_code');
        $html->setAttrib('class', 'active');
        $this->addElement($html);
        
        $css = new Zend_Form_Element_Textarea('css_code');
        $css->setAttrib('class', 'hide');
        $this->addElement($css);
        
        $js = new Zend_Form_Element_Textarea('js_code');
        $js->setAttrib('class', 'hide');
        $this->addElement($js);
               
        $submit = new Zend_Form_Element_Submit('sprawdz');
        $submit->setLabel('Sprawdz')
            ->setAttrib('class', 'btn btn-warning')
            ->setIgnore(false);
        $this->addElement($submit);
    }


}

