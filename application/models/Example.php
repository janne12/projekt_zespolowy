<?php

class Application_Model_Example extends Application_Model_DbTable_Ex
{
  
public function getContent($id){
    
    $db = $this->getAdapter();
    $content = $db->select()
            ->from(array('e' => 'examples.ex'), array('content_html', 'content_css', 'content_js'))
            ->where('id = ?', $id);
    $content = $db->fetchRow($content);
    return $content;
    
}

public function getRoute($params){
    
    $db = $this->getAdapter();
    $route = $db->select()
                ->from(array('e' => 'examples.ex'), array('id'))
                ->where('route = ?', $params);
    $route = $db->fetchRow($route);
    return $route;
    
}
    
}

