<?php

class IndexController extends Zend_Controller_Action
{

    protected $model = null;

    public function init()
    {
        $this->model = new Application_Model_Example();
    }

    public function indexAction()
    {
        
        if($this->getRequest()->getPost()){
            $post = $this->getRequest()->getPost();
           // var_dump($post);exit;
            $this->sprawdzAction($post);
        }
        else{
        $params = $this->getRequest()->getRequestUri();
        $path = realpath(APPLICATION_PATH . '/../public/cache/cache.html');
        echo $path;
        $this->view->path = $path;
        $route = $this->model->getRoute($params);
        if(isset($route['id'])){
        $id = $route['id'];
        $content = $this->model->getContent($id);
        
            $form = new Application_Form_Example;
            $this->view->form = $form;
            $form->getElement('html_code')->setValue($content['content_html']);
            $form->getElement('css_code')->setValue($content['content_css']);
            $form->getElement('js_code')->setValue($content['content_js']);
        }
        else{
            $this->_redirect('/404');
        }
        }
    }

    public function sprawdzAction($post)
    {
        $form = new Application_Form_Example;
        $this->view->form = $form;
        $form->getElement('html_code')->setValue($post['html_code']);
            $form->getElement('css_code')->setValue($post['css_code']);
            $form->getElement('js_code')->setValue($post['js_code']);
        $this->view->iframe = $post['html_code'].$post['css_code'].$post['js_code'];
    }

    public function notfoundAction()
    {
        //$this->_redirect('/404');
        $this->view->error = "<h1>STRONA NIE ISTNIEJE</h1>";
    }


}





